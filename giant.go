package main

import ("fmt"
		"strings")

func main() {

	// solicita nome para o usuário
	name    := askString("Nome: ")

	// verifica se a string contém chars invalidos
	if (!validateString(name)) {

		fmt.Println("Bye! ")
		return
	}

	// solicita altura para o usuário
	heigth  := askString("Altura: ")

	// solicita numero de acompanhantes
	friends := askString("Amigos: ")
	
	fmt.Println("Olá ", name, "sua altura é de", heigth, "m", "está com", friends, "amigos")

	return
}

func askString(message string) string {

	fmt.Print(message)

	var input string

	fmt.Scanln(&input)

	return input
}


// Função que verifica se todos os valores 
// são pertencentes ao alfabeto
func validateString(s string) bool {

	return !strings.ContainsAny(s, "1234567890&!@#$%*()-:><.,")
}
