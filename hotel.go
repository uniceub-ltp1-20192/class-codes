// No hotel morte lenta, estavam hospedados 5 hospédes, Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
// A meia noite, o hóspede do quarto 101 foi encontrado morto e os 5 suspeitos são os citados acima.
// Descubra quem é o assassino sabendo que:
//        Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é u ou o, ele pode ser o assassino.
//        Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
//        Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.
// Sabendo dessas informações acima, projete o algorítmo que fornecido o nome do suspeito, indique se ele pode ou não ser o assassino.

package main

import ("fmt"
		"strings")

func main() {

	// solcitar nome ao usuário
	name := askString("Nome: ")

	// solcitar nome ao usuário
	sobre := askString("Sobrenome: ")

	// solcitar genero ao user
	gender := askString("Genero: ")

	// verifica se o nome é uma string
	vn := checkString(name, "1234567890!@#$%*()<>.,:;?/")

	// verifica se o nome contains 'O' ou 'U'
	vv := checkString(name, "ou")

	if !vn || !vv {
		fmt.Println("Não sabe nem o que é um nome")
		return
	}

	fmt.Println("Seja bem-vindo", name, sobre, "do gênero biológico", gender)
}

func askString(msg string) string {

	// declara a var input
	var input string

	// apresenta a string Nome on-the-face
	fmt.Print(msg)
    
    // lê o que o usuário digitou
    fmt.Scanln(&input)

    return input
}

func checkString(s string, chars string) bool {
	return !strings.ContainsAny(s, chars)
}