# solicita a quantidade de notas
num_grades = int(input("Quantidade de notas: "))

# variaveis para armazenar a soma das notas
# a soma das notas com peso e a soma dos pesos 
sum_grades          = 0
sum_weighted_grades = 0
sum_weighted        = 0

# variavel para controlar o loop
#i = 1

# para a quantidade de notas solicita a nota e peso
for i in range(num_grades):
	
	grade  = float(input("Nota: "))
	weight = int(input("Peso (1-5): "))

	# adiciona a soma das notas
	sum_grades = sum_grades + grade

	# adiciona a soma das notas com peso
	sum_weighted_grades = sum_weighted_grades + (grade * weight)

	# adiciona a soma de pesos
	sum_weighted = sum_weighted + weight


# apresenta o resultado final
print("\n")
print("-----------------------------------------------")
print("Média: ", sum_grades/num_grades)
print("Média ponderada: ", sum_weighted_grades/sum_weighted) 	
