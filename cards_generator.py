from random import shuffle

SUITS          = ("SPADES", "HEARTS", "DIAMONDS", "CLUBS")
CARD_MAX_VALUE = 13
CARD_MIN_VALUE = 1

def generateCardDeck():
	
	cards = []

	for suit in SUITS:
		for value in range(CARD_MIN_VALUE, CARD_MAX_VALUE + 1):
			card = (suit, value)
			cards.append(card)

	return cards

def generateShuffleDeck():
		deck = generateCardDeck()
		shuffle(deck)
		return deck

print(generateShuffleDeck()) 