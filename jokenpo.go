package main

import ("math/rand"
		"fmt"
		"time")

func main() {

	_   = askString("Nome: ")
	user_option := askString("Pedra, Papel ou Tesoura? ") 

	// if (!isNameValid(user_name) || !isOptionValid(user_option)) {
	// 	return 
	// }

	cpu_option := generateRandomOption()

	fmt.Printf("User: %v  CPU: %v \n", user_option, cpu_option)

	result := getResult(user_option, cpu_option)

	printResult(result)
}

func askString(message string) string {

	fmt.Print(message)

	var input string

	fmt.Scanln(&input)

	return input
}

func generateRandomOption() string {

	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	number := r.Intn(3) + 1

	if (number == 1) {
		return "pedra"
	}

	if (number == 2) {
		return "papel"
	}

	return "tesoura"
}

func getResult(ui string, ci string) string {

	if (ui == ci) {
		return "ninguem"
	}

	if (ui == "pedra" && ci == "tesoura" || ui == "tesoura" && ci == "papel" || ui == "papel" && ci == "pedra") {
		return "você"
	}

	return "cpu"
}

func printResult(result string) {

	fmt.Println("O vitorioso foi: " + result)
}