
# solicita a quantidade de produtos
num_products = int(input("Quantidade de produtos: "))

# variaveis para armazenar o valor total e o valor com desconto
total          = 0
total_discount = 0 

# para a quantidade de produtos solicita preço e porcentagem
for prod in range(num_products):
	value    = float(input("Valor: "))
	discount = float(input("Desconto (0-1): "))

	# adiciona ao total o valor do produto
	total = total + value

	# adiciona ao total com desconto o valor descontado
	total_discount = total_discount + (value * (1 - discount))


# apresenta o resultado final
print("\n")
print("-----------------------------------------------")
print("Você pagaria: ", total)
print("Vai pagar: ", total_discount) 	