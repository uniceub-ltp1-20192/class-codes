from Entidades.cachorro import Cachorro

class Pug(Cachorro):
  def __init__(self,tipoDeRabo = "enrolado"):
    super().__init__()
    self._tipoDeRabo = tipoDeRabo


  @property
  def tipoDeRabo(self):
    return self._tipoDeRabo

  @tipoDeRabo.setter
  def tipoDeRabo(self,tipoDeRabo):
    self._tipoDeRabo = tipoDeRabo

  def latir(self):
    print("auuu tshi auu tshi")
   
    # self._tamanho = tamanho
    # self._raca = raca
    # self._cor = cor
    # self._peso = peso

  def __str__(self):
    return """Dados de Pug:
  Identificador: {} 
  Tamanho: {}
  Raca: {}
  Cor: {}
  Peso: {}
  Tipo de Rabo: {}
    """.format(self.identificador
    ,self.tamanho,
    self.raca,
    self.cor,
    self.peso,
    self.tipoDeRabo)