from Validador.validador import Validador
from Dados.dados import Dados
from Entidades.pug import Pug

class Menu:
    @staticmethod
    def menuPrincipal():
        print("""
            0 - Sair
            1 - Consultar
            2 - Inserir
            3 - Alterar
            4 - Deletar 
            """)
        return Validador.validar("[0-4]",
        """Opcao do menu deve estar entre {}""",
        """Opcao {} Valida!""")
        

    @staticmethod
    def menuConsultar():
        return input("""
                0 - Voltar
                1 - Consultar por identificador
                2 - Consultar por propriedade
                    """)

    @staticmethod
    def iniciarMenu():
        opMenu = ""
        d = Dados()
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()
            if opMenu == "1":
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            print(retorno)
                        else:
                            print("Nao encontrado")
                    elif opMenu == "2":
                        retorno = Menu.menuBuscarPorAtributo(d)
                        if retorno != None:
                            print(retorno)
                        else:
                            print("Nao encontrado")
                    elif opMenu == "0":
                        print("Saindo")
                    else:
                        print("Digite uma opcao valida!")  
                opMenu = ""            
            elif opMenu == "2":
                print("Entrei em Inserir")
                Menu.menuInserir(d)
                opMenu = ""
            elif opMenu == "3":
                print("Entrei em Alterar")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)
                        else:
                            print("Nao encontrado")

                    elif opMenu == "2":
                        retorno = Menu.menuBuscarPorAtributo(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)
                        else:
                            print("Nao encontrado")
                    elif opMenu == "0":
                        print("Saindo")
                    else:
                        print("Digite uma opcao valida!")
                opMenu = ""
            elif opMenu == "4":
                print("Entrei em Deletar")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        Menu.menuDeletar(d,retorno)
                        
                    elif opMenu == "2":
                        retorno = Menu.menuBuscarPorAtributo(d)
                        Menu.menuDeletar(d,retorno)
                        
                    elif opMenu == "0":
                        print("Saindo")
                    else:
                        print("Digite uma opcao valida!") 
                opMenu = ""
            elif opMenu == "0":
                print("Saindo")
            else:
                print("Digite uma opcao valida!")

    @staticmethod
    def menuInserir(d):   
        pug = Pug()    
        pug.peso = input("Informe um peso:")
        pug.cor = input("Informe uma cor:")
        pug.tamanho = input("Informe o tamanho:")
        pug.tipoDeRabo = input("Informe o Tipo de Rabo:")
        pug.raca = input("Informa a raca:")
        d.inserirDado(pug)

    @staticmethod
    def menuAlterar(retorno,d):   
        # Crie um validador para receber o valor de cada entrada
        # e nao alterar caso o valor informado seja vazio 
        # "" ou None
        print(retorno)
        retorno.peso = Validador.validarValorEntrada(retorno.peso,"Informe um peso:") 
        retorno.cor =  Validador.validarValorEntrada(retorno.cor,"Informe uma cor:") 
        retorno.tamanho = Validador.validarValorEntrada(retorno.tamanho,"Informe o tamanho:") 
        retorno.tipoDeRabo = Validador.validarValorEntrada(retorno.tipoDeRabo,"Informe o Tipo de Rabo:") 
        retorno.raca = Validador.validarValorEntrada(retorno.raca,"Informa a raca:") 
      
        d.alterarDado(retorno)


    @staticmethod
    def menuDeletar(d,entidade):
        print(entidade)
        resposta = input(
            """Deseja deletar o pug ?
            S - Sim
            N - Nao""")
        if(resposta == "S" or  resposta == "s"):
            d.deletar(entidade)


    @staticmethod
    def menuBuscaPorIdentificador(d):        
        retorno = d.buscarPorIdentificador(
            Validador.validar(r'\d+','',''))
        return retorno

    @staticmethod
    def menuBuscarPorAtributo(d : Dados):
        retorno = d.buscarPorAtributo(
            input("Informe um tamanho"))
        print(retorno)
